﻿using OOP.Chapter;
using System;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.Encapsulation   
            Encapsulation encapsulationExample = new Encapsulation(20);
            encapsulationExample.MySquare();

            //2.Inheritance   
            Inheritance inheritanceExample = new Inheritance();
            inheritanceExample.Run();

            //3.Polymorphism   
            Polymorphism polymorphismExample = new Polymorphism();
            polymorphismExample.Run();

            //4.Polymorphism   
            Abstraction abstractionExample = new Abstraction();
            abstractionExample.Run();

            Console.ReadLine();
        }
    }
}