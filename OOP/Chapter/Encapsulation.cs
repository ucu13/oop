﻿using System;

namespace OOP.Chapter
{
    class Encapsulation
    {
        /// <summary>    
        /// Every member Variable and Function of the class are bind    
        /// with the Encapsulation class object only and safe with     
        /// the outside inference    
        /// </summary>    

        // Encapsulation Begin    
        int Size;

        //class constructor    
        public Encapsulation(int size)
        {
            this.Size = size;
        }

        //calculating the square    
        public void MySquare()
        {
            int calc = Size * Size;
            Console.WriteLine(calc);
        }

        // End of Encapsulation    
    }
}