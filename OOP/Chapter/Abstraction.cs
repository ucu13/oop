﻿using System;

namespace OOP.Chapter
{

    abstract class Animal
    {
        public abstract void Eat();
        public void Sound()
        {
            Console.WriteLine("animal can sound");
        }
    }
    class Dog : Animal
    {
        public override void Eat()
        {
            Console.WriteLine("dog can eat");
        }
    }

    class Abstraction
    {
        public void Run()
        {
            Dog mydog = new Dog();
            Animal thePet = mydog;

            thePet.Eat();
            mydog.Sound();
        }
    }
}