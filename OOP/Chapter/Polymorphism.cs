﻿using System;

namespace OOP.Chapter
{
    public abstract class Employee
    {
        public virtual void LeaderName()
        {
        }
    }

    public class HrDepart : Employee
    {
        public override void LeaderName()
        {
            Console.WriteLine("Mr. jone");
        }
    }
    public class ItDepart : Employee
    {
        public override void LeaderName()
        {
            Console.WriteLine("Mr. Tom");
        }
    }

    public class FinanceDepart : Employee
    {
        public override void LeaderName()
        {
            Console.WriteLine("Mr. Linus");
        }
    }

    class Polymorphism
    {
        public void Run()
        {
            HrDepart hrDepart = new HrDepart();
            ItDepart itDepart = new ItDepart();
            FinanceDepart financeDepart = new FinanceDepart();

            hrDepart.LeaderName();
            itDepart.LeaderName();
            financeDepart.LeaderName();
        }
    }
}